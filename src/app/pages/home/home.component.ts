import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { Category } from 'src/app/helpers/enums/Category';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  data: any = {
    projects: {}
  };

  Category = Category;

  model: any = {};
  Message = "";
  ErrorMessage = "";
  loading = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    // this.data.projects = this.getProjects();
  }

  // submit() {
  //   let dto = this.checkCreate(this.type);
  //   this.lotteryService.create(dto)
  //     .subscribe(
  //       data => {
  //         if (data != null) {
  //           this.Message = "Record added succesfully!";
  //         } else {
  //           this.ErrorMessage = "Something went wrong!";
  //         }
  //         this.loading = false;
  //       }
  //     );
  // }
}

@Pipe({
  name: 'enumToArray'
})
export class EnumToArrayPipe implements PipeTransform {
  transform(data: Object) {
    return Object.keys(data);
  }
}